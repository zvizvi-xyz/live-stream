'use strict';

let gulp = require('gulp');
const webpack = require('webpack');
const path = require('path');
const sync = require('run-sequence');
const rename = require('gulp-rename');
const template = require('gulp-template');
const fs = require('fs');
const yargs = require('yargs');
const _ = require('lodash');
const gutil = require('gulp-util');
const serve = require('browser-sync');
const del = require('del');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const colorsSupported = require('supports-color');
const historyApiFallback = require('connect-history-api-fallback');

const upCase = val => _.upperFirst(_.camelCase(val));
const kebabCase = val => _.kebabCase(val);

const root = 'client';

// helper method for resolving paths
const resolveToApp = (glob = '') => {
  return path.join(root, glob); // app/{glob}
};

const resolveToComponents = (glob = '') => {
  return path.join(root, '/app/components', glob); // app/components/{glob}
};
const resolveToServices = (glob = '') => {
  return path.join(root, '/app/services', glob); // app/components/{glob}
};
const resolveToDirective = (glob = '') => {
  return path.join(root, '/app/directives', glob); // app/components/{glob}
};

// map of all paths
function paths () {
  return {
    js: resolveToComponents('**/*!(.spec.js).js'), // exclude spec files
    scss: resolveToApp('**/*.scss'), // stylesheets
    html: [
      resolveToApp('**/*.html'),
      path.join(root, '/index.html')
    ],
    entry: [
      'babel-polyfill',
      path.join(__dirname, root, '/app/app.js')
    ],
    output: root,
    blankTemplatesCmp: path.join(__dirname, 'generator', 'component/**/*.**'),
    blankTemplatesService: path.join(__dirname, 'generator', 'service/**/*.**'),
    blankTemplatesDirective: path.join(__dirname, 'generator', 'directive/**/*.**'),
    dest: path.join(__dirname, 'dist')
  };
}

gulp.task('clean', () => {
  return del([paths().dest])
    .then((paths) => {
      gutil.log('[clean]', paths);
    });
});

// use webpack.config.js to build modules
gulp.task('webpack', (done) => {
  const config = require('./webpack.dist.config');
  config.entry.app = paths().entry;

  webpack(config, (err, stats) => {
    if (err) {
      throw new gutil.PluginError('webpack', err);
    }

    gutil.log('[webpack]', stats.toString({
      colors: colorsSupported,
      chunks: false,
      errorDetails: true
    }));

    done();
  });
});

gulp.task('build', gulp.series('clean', 'webpack'));

gulp.task('serve', () => {
  const config = require('./webpack.dev.config');
  config.entry.app = [
    // this modules required to make HRM working
    // it responsible for all this webpack magic
    'webpack-hot-middleware/client?reload=true'
    // application entry point
  ].concat(paths().entry);
  const compiler = webpack(config);

  serve({
    port: process.env.PORT || 3000,
    open: false,
    server: { baseDir: root + '/' },
    ghostMode: {
      clicks: false,
      forms: false,
      scroll: false
    },
    serveStatic: [
      {
        route: '/assets',
        dir: './client/assets'
      },
      {
        route: '/vendors',
        dir: './client/vendors'
      }
    ],
    middleware: [
      historyApiFallback(),
      webpackDevMiddleware(compiler, {
        stats: {
          colors: colorsSupported,
          chunks: false,
          modules: false
        },
        publicPath: config.output.publicPath
      }),
      webpackHotMiddleware(compiler)
    ]
  });
});

gulp.task('watch', gulp.series('serve'));

gulp.task('component', () => {
  const name = yargs.argv.name;
  const moduleName = yargs.argv.module || name;
  const parentPath = yargs.argv.parent || '';
  const destPath = path.join(resolveToComponents(), kebabCase(parentPath), kebabCase(name));

  return gulp.src(paths().blankTemplatesCmp)
    .pipe(template({
      name: name,
      moduleName: moduleName,
      upCaseName: upCase(name),
      kebabCaseName: kebabCase(name)
    }))
    .pipe(rename((path) => {
      path.basename = path.basename.replace('temp', kebabCase(name));
    }))
    .pipe(gulp.dest(destPath));
});

gulp.task('service', () => {
  const name = yargs.argv.name;
  const moduleName = yargs.argv.module || name;
  const parentPath = yargs.argv.parent || '';
  const destPath = path.join(resolveToServices(), kebabCase(parentPath), kebabCase(name));

  return gulp.src(paths().blankTemplatesService)
    .pipe(template({
      name: name,
      moduleName: moduleName,
      upCaseName: upCase(name),
      kebabCaseName: kebabCase(name)
    }))
    .pipe(rename((path) => {
      path.basename = path.basename.replace('temp', kebabCase(name));
    }))
    .pipe(gulp.dest(destPath));
});

gulp.task('directive', () => {
  const name = yargs.argv.name;
  const moduleName = yargs.argv.module || name;
  const parentPath = yargs.argv.parent || '';
  const destPath = path.join(resolveToDirective(), kebabCase(parentPath), kebabCase(name));

  return gulp.src(paths().blankTemplatesDirective)
    .pipe(template({
      name: name,
      moduleName: moduleName,
      upCaseName: upCase(name),
      kebabCaseName: kebabCase(name)
    }))
    .pipe(rename((path) => {
      path.basename = path.basename.replace('temp', kebabCase(name));
    }))
    .pipe(gulp.dest(destPath));
});

gulp.task('default', gulp.series('watch'));
