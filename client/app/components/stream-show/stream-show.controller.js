import _ from 'lodash';

export default function ($scope, $state, $window, $timeout, streamsService, googleAnalyticsService) {
  let vm;

  class StreamShowController {
    constructor () {
      vm = this;
      vm.name = 'showStream';
    }

    $onInit () {
      vm.streamName = _.get($state, 'params.streamName');
      streamsService.getStreamByName(vm.streamName)
        .then((stream) => {
          vm.stream = stream;

          $timeout(() => {
            if (vm.stream.type === 'hls' && $window.Hls.isSupported()) {
              vm.hlsLoadSource(vm.stream.name, vm.stream.source);
            } else if (vm.stream.type === 'iframe') {
              vm.setupIframe(vm.stream.source);
            } else if (vm.stream.type === 'video') {
              vm.setupVideo(vm.stream.name, vm.stream.source);
            }
          });
          googleAnalyticsService.setPage(vm.stream.label);
        });
    }

    hlsLoadSource (item, url) {
      let Hls = $window.Hls;
      let video = document.getElementById(item);
      let hls = new Hls();
      hls.loadSource(url);
      hls.attachMedia(video);
      hls.on(Hls.Events.MANIFEST_PARSED, function () {
        video.play();
      });
    }

    setupVideo (item, url) {
      let video = document.querySelector('video.video-player');
      if (video) {
        video.src = vm.stream.source;
      }
    }

    setupIframe (url) {
      let iframe = document.querySelector('iframe.video-iframe');
      if (iframe) {
        iframe.src = url;
      }
    }
  }

  return new StreamShowController();
}
