import template from './stream-show.html';
import controller from './stream-show.controller';
import './stream-show.scss';

let streamShowComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

controller.$inject = ['$scope', '$state', '$window', '$timeout', 'streamsService', 'googleAnalyticsService'];

export default streamShowComponent;
