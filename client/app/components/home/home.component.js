import template from './home.html';
import controller from './home.controller';
import './home.scss';

let homeComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

controller.$inject = ['$scope', 'streamsService', 'googleAnalyticsService'];

export default homeComponent;
