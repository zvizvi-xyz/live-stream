export default function () {
  let vm;

  class FooterController {
    constructor () {
      vm = this;
      vm.name = 'footer';
    }
  }

  return new FooterController();
}
