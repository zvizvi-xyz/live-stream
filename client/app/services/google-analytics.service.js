import angular from 'angular';

let $window;
let $location;

class googleAnalyticsService {
  constructor (_$window, _$location) {
    $window = _$window;
    $location = _$location;
  }

  setPage (title) {
    $window.document.title = 'שידור חי' + (title ? ' - ' + title : '');
    $window.gtag('config', $window.GA_TRACKING_ID, { 'page_path': $location.$$url });
    document.querySelector('link[rel=canonical]').href = 'https://kotel-cams.ml' + $location.$$url;
  }
}

googleAnalyticsService.$inject = ['$window', '$location'];

let googleAnalyticsModule = angular.module('googleAnalyticsService', [])
  .service('googleAnalyticsService', googleAnalyticsService)
  .name;

export default googleAnalyticsModule;
