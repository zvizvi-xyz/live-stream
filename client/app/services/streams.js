import angular from 'angular';
import _ from 'lodash';

let $http;

class streamsService {
  constructor (_$http) {
    $http = _$http;
    // loadStreams(this);
    // loadCategories(this);
  }

  getStreams () {
    if (_.get(this, 'streams.length')) {
      return Promise.resolve(this.streams);
    }
    return loadStreams(this)
      .then(() => {
        return this.getStreams();
      });
  }

  getStreamByName (name) {
    if (_.get(this, 'streams.length')) {
      let stream = this.streams.filter(stream => stream.name === name)[0];
      return Promise.resolve(stream);
    }
    return loadStreams(this)
      .then(() => {
        return this.getStreamByName(name);
      });
  }

  getStreamCategories () {
    if (_.get(this, 'categories.length')) {
      return Promise.resolve(this.categories);
    }
    return loadCategories(this)
      .then(() => {
        return this.getStreamCategories();
      });
  }
}

streamsService.$inject = ['$http'];

let streamsModule = angular.module('streamsService', [])
  .service('streamsService', streamsService)
  .name;

export default streamsModule;

function loadStreams (service) {
  return $http({
    url: '/data/streams.json'
  })
    .then((result) => {
      let streams = result.data;
      service.streams = streams;
      return streams;
    });
}

function loadCategories (service) {
  return $http({
    url: '/data/categories.json'
  })
    .then((result) => {
      let categories = result.data;
      service.categories = categories;
      return categories;
    });
}
